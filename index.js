// const { PDFDocument } = PDFLib
import { PDFDocument } from 'pdf-lib'
// var admin = require("firebase-admin");
import admin from 'firebase-admin';
import auth from 'google-auth-library';
import express from 'express';
const app = express()
const PORT = process.env.PORT || 3000;
import bodyParser from 'body-parser';
import https from "https";
import fetch from "node-fetch";
import cors from 'cors';
import key from './referror-messaging-firebase-adminsdk-ypiyt-44f6e1e61e.json' assert  { type: 'json' };

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());
app.use(cors())

admin.initializeApp({
    credential: admin.credential.cert(key),
});

app.get('/get_pdf', (req, res) => {
    res.send("https://5.imimg.com/data5/SELLER/Doc/2021/4/QI/LK/YD/7115850/pdf-conversion-services.pdf")
})

app.post('/get_all_fields', async (req, res) => {
    var pdfUrl = req.body["pdf_url"];
    const formUrl = pdfUrl;
    const formPdfBytes = await fetch(formUrl).then(res => res.arrayBuffer())

    // Load a PDF with form fields
    const pdfDoc = await PDFDocument.load(formPdfBytes)

    // Get the form containing all the fields:
    var fieldList = [];
    const form = pdfDoc.getForm()
    const fields = form.getFields()
    fields.forEach(field => {
        const type = field.constructor.name
        const name = field.getName()
        // console.log(`${type}: ${name}`)
        fieldList.push(name)
    })

    console.log(fieldList);
    res.status(200).send({
        "fields": fieldList
    })
})

app.post('/fill_pdf', async (req, res) => {
    console.log('Got body:', req.body);
    var data = req.body;

    var pdfUrl = req.body["pdf_url"];
    const formUrl = pdfUrl;
    const formPdfBytes = await fetch(formUrl).then(res => res.arrayBuffer())

    // Load a PDF with form fields
    const pdfDoc = await PDFDocument.load(formPdfBytes);

    // Get the form containing all the fields:
    const form = pdfDoc.getForm()
    const fields = form.getFields()

    fields.forEach(field => {
        const type = field.constructor.name
        const name = field.getName()

        if (name in data) {
            const theField = form.getTextField(name)
            theField.setText(data[name])
        }
    })
    const pdfBytes = await pdfDoc.save()

    res.writeHead(200, {
        'Content-Type': "application/pdf",
        'Content-disposition': 'attachment;filename=' + "pdf-lib_form_creation_example.pdf",
        'Content-Length': pdfBytes.length
    });
    res.end(Buffer.from(pdfBytes, 'binary'));

});

app.post("/send_text_message", async (req, res) => {
    let data = req.body;

    let message = data["message"];
    let destination = data["to_token"];

    const messaging = admin.messaging()
    const androidConfig = {
        priority: "high",
    };
    var payload = {
        notification: {
            title: "Referror Messaging",
            body: message,
        },
        token: destination,
        android: androidConfig,
    };

    messaging.send(payload)
        .then((result) => {
            res.send(result)
        }).catch((error) => {
            res.send(error);
        });
})

app.post("/send_message_to_topic", async (req, res) => {
    let data = req.body;

    let message = data["message"];
    let topic = data["topic"];

    const messaging = admin.messaging()
    var payload = {
        notification: {
            title: "Referror Messaging",
            body: message,
        },
        topic: topic
    };

    messaging.send(payload)
        .then((result) => {
            res.send(result)
        }).catch((error) => {
            res.send(error);
        });
})

app.get('/get_google_api_token', async (req, res) => {
    const jwtClient = new auth.JWT(
        key.client_email,
        null,
        key.private_key,
        "https://www.googleapis.com/auth/firebase.messaging",
        null
    );
    jwtClient.authorize(function (err, tokens) {
        if (err) {
            res.send(err);
            return;
        }
        res.end(tokens.access_token);
    });
});


app.listen(PORT, () => {
    console.log(`Example app listening on port ${PORT}`)
})

function getAccessToken() {
    return new Promise(function (resolve, reject) {
        const key = require('../placeholders/service-account.json');
        const jwtClient = new google.auth.JWT(
            key.client_email,
            null,
            key.private_key,
            SCOPES,
            null
        );
        jwtClient.authorize(function (err, tokens) {
            if (err) {
                reject(err);
                return;
            }
            resolve(tokens.access_token);
        });
    });
}
